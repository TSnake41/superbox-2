/*

 SuperBox - A graphical console server for batchs
 Copyright (c) 2014, 2015 Romain GARBI (Darkbatcher)

 All rights reserved.
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 * Neither the name of the name of Romain Garbi (Darkbatcher) nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY ROMAIN GARBI AND CONTRIBUTORS ``AS IS'' AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES
 LOSS OF USE, DATA, OR PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "superbox.h"

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <tchar.h>
#include <wchar.h>

typedef struct {
int newmode;
} _startupinfo;

extern int __getmainargs(int*, char***, char***, int, _startupinfo*);
extern int __wgetmainargs(int, wchar_t***, wchar_t***, int, STARTUPINFO*);

#if defined(_UNICODE) || defined(UNICODE)
#define __tgetmainargs(argc, argv, env, dowildcard, stinfo) \
		__wgetmainargs(argc, argv, env, dowildcard, stinfo)
#else
#define __tgetmainargs(argc, argv, env, dowildcard, stinfo) \
		__getmainargs(argc, argv, env, dowildcard, stinfo)
#endif // _UNICODE || UNICODE

extern int main(int argc, TCHAR* argv[]);

int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevious, LPSTR lpCmd, int nCmdShow)
{
	int argc;
	TCHAR **argv, **env;
	_startupinfo stinfo;

	__tgetmainargs(&argc, &argv, &env, 0, &stinfo);

	return main(argc, argv);
}
