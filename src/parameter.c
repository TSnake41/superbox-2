/*

 SuperBox - A graphical console server for batchs
 Copyright (c) 2014, 2015 Romain GARBI (Darkbatcher)

 All rights reserved.
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 * Neither the name of the name of Romain Garbi (Darkbatcher) nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY ROMAIN GARBI AND CONTRIBUTORS ``AS IS'' AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL ROMAIN GARBI AND CONTRIBUTORS BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES
 LOSS OF USE, DATA, OR PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
#include "superbox.h"

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <tchar.h>
#include <wchar.h>

#include "parameter.h"

int param_get(TCHAR* str, TCHAR** start, TCHAR** end)
{
	TCHAR *ret=NULL, *token, *tmp;
	int    quotes = 0;


	while (*str == TEXT(' ') || *str == TEXT('\t')
			|| *str == TEXT('\n') || *str == TEXT('\r'))
		str ++;

	if (*str == TEXT('\0'))
		return 0;

	if (*str == TEXT('"')) {

		quotes = 1;
		str ++;

	}

	token = str;

	while (1) {

		if (!(tmp = _tcspbrk(token, quotes ? TEXT("\"^") : TEXT("\" ^\t\n\r")))) {

			ret = token;
			while (*ret != TEXT('\0'))
				ret ++;

			break;

		}

		token = tmp;

		if (*token == TEXT('^') && *(++token)) {

			token ++;

		} else if (*token != TEXT('\"')) {
			ret = token;
			break;
		}

		quotes = !quotes; /* change se state of quotes research */

		token ++;

		if (*token == TEXT(' ') || *token == TEXT('\t') ||
			*token == TEXT('\r') || *token == TEXT('\n')) {

			ret = token - 1;
			break;

		}

	}

	*start = str;
	*end = ret;

	return 1;
}


void param_unescape(TCHAR* str)
{
	TCHAR* unescape=_tcschr(str, TEXT('L'));

	if (unescape == NULL)
		return;

	str = unescape + 1;
	*unescape= *str;

	unescape ++;
	str ++;

	while (*str != TEXT('\0')) {

		if (*str == TEXT('^'))
			str ++;

		*unescape = *str;
		unescape ++;
		str ++;

	}

	*unescape = TEXT('\0');
}
