/*

 SuperBox - A graphical console server for batchs
 Copyright (c) 2014, 2015 Romain GARBI (Darkbatcher)

 All rights reserved.
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 * Neither the name of the name of Romain Garbi (Darkbatcher) nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY ROMAIN GARBI AND CONTRIBUTORS ``AS IS'' AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL ROMAIN GARBI AND CONTRIBUTORS BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES
 LOSS OF USE, DATA, OR PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
#ifndef GRAPHICS_H
#define GRAPHICS_H

#include "superbox.h"

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <tchar.h>
#include <wchar.h>

#include "server.h"

extern HANDLE hOut;

typedef struct {
	TCHAR* name;
	TCHAR*(*fn)(TCHAR*);
} verb_t;

void graphics_init(void);
void graphics_exec(TCHAR* request);

int  vb_cmp(const void* v1, const void* v2);

/* define verbs prototype */
#define VBD(verb) TCHAR* vb_##verb (TCHAR* request)

#define VB_PARAM(param) \
	if (!param_get(request, &param, &request)) \
		return request; \
	if (*request) { \
		*request = TEXT('\0'); \
		request ++; \
	}

VBD(ascii);
VBD(clear);
VBD(clearrect);
VBD(cleartext);
VBD(cursorheight);
VBD(color);
VBD(colorrect);
VBD(goto);
VBD(display);
VBD(displaytext);
VBD(hidecursor);
VBD(newline);
VBD(offset);
VBD(quit);
VBD(showconsole);
VBD(title);

#endif
