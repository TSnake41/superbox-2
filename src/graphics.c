/*

 SuperBox - A graphical console server for batchs
 Copyright (c) 2014, 2015 Romain GARBI (Darkbatcher)

 All rights reserved.
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 * Neither the name of the name of Romain Garbi (Darkbatcher) nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY ROMAIN GARBI AND CONTRIBUTORS ``AS IS'' AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL ROMAIN GARBI AND CONTRIBUTORS BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES
 LOSS OF USE, DATA, OR PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
#include "superbox.h"

#define _WIN32_WINNT 0x0500

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <tchar.h>
#include <wchar.h>

#include "server.h"
#include "graphics.h"

HANDLE hOut;
HANDLE hCon;

#define t(_1, _2, fn) {TEXT("/" _1), fn}, {TEXT("/" _2),fn}
#define u(_1, fn) {TEXT("/" _1), fn}
#define VERBSIZE (sizeof(verbs)/sizeof(verb_t))

/* The verbs must be in lexicographical order */
verb_t verbs[] = {
	t("a", "ascii", vb_ascii),
	u("c", vb_color),
	t("cl", "clear", vb_clear),
	u("clearrect", vb_clearrect),
	u("cleartext", vb_cleartext),
	u("clr", vb_clearrect),
	u("color", vb_color),
	u("colorrect", vb_colorrect),
	t("ch", "cursorheight", vb_cursorheight),
	u("cr", vb_colorrect),
	u("ct", vb_cleartext),
	t("d", "display", vb_display),
	t("displaytext", "dt", vb_displaytext),
	t("g", "goto", vb_goto),
	t("h", "hidecursor", vb_hidecursor),
	t("n", "newline", vb_newline),
	t("o", "offset", vb_offset),
	t("q", "quit", vb_quit),
	t("s", "showconsole", vb_showconsole),
	t("t", "title", vb_title)
};

/* memorize last goto statement */
int lastx = 0, lasty = 0;

/* memorize offsets */
int offsetx = 0, offsety = 0;

void graphics_init(void)
{
	hOut = GetStdHandle(STD_OUTPUT_HANDLE);

	if (hOut == INVALID_HANDLE_VALUE)
		ERR("Graphics", "Unable to get output handle (%d).\n",
				GetLastError());

	if (!(hCon = GetConsoleWindow()))
		ERR("Graphics", "Unable to get console handle (%d).\n",
				GetLastError());

}

void graphics_exec(TCHAR* request)
{
	TCHAR *param, *end;
	verb_t key={NULL,NULL}, *ret;

	while (param_get(request, &param, &end)) {

		if (*end) {

			*end = TEXT('\0');
			end ++;

		}

		key.name = param;

		ret = bsearch(&key, verbs, VERBSIZE, sizeof(verb_t), vb_cmp);

		if (ret != NULL)
			end=ret->fn(end);

		request = end;

	}
}

int  vb_cmp(const void* p1, const void* p2)
{
	verb_t *v1=(verb_t*)p1, *v2=(verb_t*)p2;

	return stricmp(v1->name, v2->name);
}

VBD(ascii)
{
	TCHAR *nb;
	TCHAR c;
	DWORD ret;

	VB_PARAM(nb);

	c = _tcstol(nb, NULL, 0);

	WriteConsole(hOut, &c, 1, &ret, NULL);

	return request;
}

VBD(color)
{
	TCHAR* param;
	int color;

	VB_PARAM(param);

	color = _tcstol(param, NULL, 0);

	SetConsoleTextAttribute(hOut, color);

	return request;
}

VBD(goto)
{
	TCHAR *x, *y;
	COORD pos;

	VB_PARAM(x);
	VB_PARAM(y);

	pos.X = (lastx = offsetx + _tcstol(x, NULL, 0));
	pos.Y = (lasty = offsety + _tcstol(y, NULL, 0));

	SetConsoleCursorPosition(hOut, pos);

	return request;
}

VBD(display)
{
	TCHAR *text;
	DWORD len, nb;

	VB_PARAM (text);
	len = _tcslen(text);

	WriteConsole(hOut, text, len, &nb, NULL);

	return request;
}

VBD(displaytext)
{
	TCHAR *text;
	DWORD len, nb;
	CONSOLE_SCREEN_BUFFER_INFO info;


	VB_PARAM (text);
	len = _tcslen(text);

	GetConsoleScreenBufferInfo(hOut, &info);
	WriteConsoleOutputCharacter(hOut, text, len, info.dwCursorPosition, &nb);

	info.dwCursorPosition.X = (info.dwCursorPosition.X + nb) % info.dwSize.X;
	info.dwCursorPosition.Y += (info.dwCursorPosition.X + nb) / info.dwSize.X;

	SetConsoleCursorPosition(hOut, info.dwCursorPosition);

	return request;
}

VBD(cursorheight)
{
	TCHAR* height;
	CONSOLE_CURSOR_INFO info;

	VB_PARAM(height);

	GetConsoleCursorInfo(hOut, &info);
	info.dwSize = strtol(height, NULL, 0);
	SetConsoleCursorInfo(hOut, &info);

	return request;
}

VBD(hidecursor)
{
	TCHAR *mode;
	CONSOLE_CURSOR_INFO info;

	VB_PARAM(mode);

	GetConsoleCursorInfo(hOut, &info);

	info.bVisible = !strtol(mode, NULL, 0);

	SetConsoleCursorInfo(hOut, &info);

	return request;
}

VBD(newline)
{
	COORD pos;

	pos.X = lastx;
	pos.Y = ++lasty;

	SetConsoleCursorPosition(hOut, pos);

	return request;
}

VBD(offset)
{
	TCHAR *x, *y;

	VB_PARAM(x);
	VB_PARAM(y);

	offsetx = _tcstol(x, NULL, 0);
	offsety = _tcstol(y, NULL, 0);

	return request;
}

VBD(quit)
{
	ExitProcess(0);
	return request;
}

VBD(showconsole)
{
	TCHAR* state;
	int mode;

	VB_PARAM(state);

	mode = _tcstol(state, NULL, 0);

	ShowWindow(hCon, mode);

	return request;
}

VBD(clear)
{
	COORD cursor={offsetx, offsety};
	DWORD nb, consize;
	CONSOLE_SCREEN_BUFFER_INFO info;

	GetConsoleScreenBufferInfo(hOut, &info);

	consize = info.dwSize.X * info.dwSize.Y;
	FillConsoleOutputCharacter(hOut, TEXT(' '), consize, cursor, &nb);
	FillConsoleOutputAttribute(hOut, info.wAttributes, consize, cursor, &nb);

	lastx = offsetx;
	lasty = offsety;
	SetConsoleCursorPosition(hOut, cursor);

	return request;
}

VBD(clearrect)
{
	TCHAR *x, *y, *width, *height;
	int size, i;
	COORD dim;
	SMALL_RECT rect;
	CONSOLE_SCREEN_BUFFER_INFO info;
	CHAR_INFO *array, item;

	VB_PARAM(x);
	VB_PARAM(y);
	VB_PARAM(width);
	VB_PARAM(height);

	dim.X = _tcstol(width, NULL, 0);
	dim.Y = _tcstol(height, NULL, 0);

	size = dim.X*dim.Y;

	if (!(array = malloc(size*sizeof(CHAR_INFO))))
		return request;

	rect.Top = offsety + _tcstol(y, NULL, 0);
	rect.Left = offsetx + _tcstol(x, NULL, 0);
	rect.Bottom = rect.Top + dim.Y - 1;
	rect.Right = rect.Top + dim.X - 1;

	GetConsoleScreenBufferInfo(hOut, &info);

	#ifdef UNICODE
		item.Char.UnicodeChar = L' ';
	#else
		item.Char.AsciiChar = ' ';
	#endif

	item.Attributes = info.wAttributes;

	for (i=0;i<size;i++)
		memcpy(array+i, &item, sizeof(CHAR_INFO));

	WriteConsoleOutput(hOut, array, dim, (COORD){0,0}, &rect);

	free(array);

	return request;
}

VBD(colorrect)
{
	TCHAR *color, *x, *y, *width, *height;
	int size, i, attr;
	COORD dim;
	SMALL_RECT rect;
	CHAR_INFO *array;

	VB_PARAM(color);
	VB_PARAM(x);
	VB_PARAM(y);
	VB_PARAM(width);
	VB_PARAM(height);

	dim.X = _tcstol(width, NULL, 0);
	dim.Y = _tcstol(height, NULL, 0);

	size = dim.X*dim.Y;

	if (!(array = malloc(size*sizeof(CHAR_INFO))))
		return request;

	rect.Top = offsety + _tcstol(y, NULL, 0);
	rect.Left = offsetx + _tcstol(x, NULL, 0);
	rect.Bottom = rect.Top + dim.Y - 1;
	rect.Right = rect.Top + dim.X - 1;

	attr = _tcstol(color, NULL, 0);

	ReadConsoleOutput(hOut, array, dim, (COORD){0,0}, &rect);

	for (i=0;i<size;i++)
		(array+i)->Attributes = attr;

	WriteConsoleOutput(hOut, array, dim, (COORD){0,0}, &rect);

	free(array);

	return request;
}

VBD(cleartext)
{
	TCHAR *x, *y, *width, *height;
	int size, i;
	COORD dim;
	SMALL_RECT rect;
	CHAR_INFO *array, item;

	VB_PARAM(x);
	VB_PARAM(y);
	VB_PARAM(width);
	VB_PARAM(height);

	dim.X = _tcstol(width, NULL, 0);
	dim.Y = _tcstol(height, NULL, 0);

	size = dim.X*dim.Y;

	if (!(array = malloc(size*sizeof(CHAR_INFO))))
		return request;

	rect.Top = offsety + _tcstol(y, NULL, 0);
	rect.Left = offsetx + _tcstol(x, NULL, 0);
	rect.Bottom = rect.Top + dim.Y - 1;
	rect.Right = rect.Top + dim.X - 1;

	ReadConsoleOutput(hOut, array, dim, (COORD){0,0}, &rect);

	for (i=0;i<size;i++) {

	#ifdef UNICODE
		(array+i)->Char.UnicodeChar = L' ';
	#else
		(array+i)->Char.AsciiChar = ' ';
	#endif

	}

	WriteConsoleOutput(hOut, array, dim, (COORD){0,0}, &rect);

	free(array);

	return request;
}

VBD(title)
{
	TCHAR* title;

	VB_PARAM(title);

	SetConsoleTitle(title);

	return request;
}
