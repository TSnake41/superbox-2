/*

 SuperBox - A graphical console server for batchs
 Copyright (c) 2014, 2015 Romain GARBI (Darkbatcher)

 All rights reserved.
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 * Neither the name of the name of Romain Garbi (Darkbatcher) nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY ROMAIN GARBI AND CONTRIBUTORS ``AS IS'' AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES
 LOSS OF USE, DATA, OR PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include "superbox.h"

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <tchar.h>
#include <wchar.h>

#include "server.h"

int server(TCHAR* name, void(*fn)(TCHAR*), int type)
{
	HANDLE events[INSTANCES];
	pipeinst_t pipes[INSTANCES];

	DWORD size;

	int i, err, success;

	/* Create pipe and event objects for each instance */
	for (i=0;i < INSTANCES; i++) {

		if ((events[i] = CreateEvent(NULL, TRUE, TRUE, NULL)) == NULL)
			ERR("Server", "Unable to create new event (%d).\n",
							GetLastError());

		pipes[i].overlapped.hEvent = events[i];

		if ((pipes[i].pipe = CreateNamedPipe(name,
											 PIPE_ACCESS_INBOUND |
											 FILE_FLAG_OVERLAPPED,
											 PIPE_TYPE_MESSAGE |
											 PIPE_READMODE_MESSAGE,
											 INSTANCES,
											 BUFSIZE*sizeof(TCHAR),
											 BUFSIZE*sizeof(TCHAR),
											 PIPE_TIMEOUT,
											 NULL)) == INVALID_HANDLE_VALUE)
			ERR("Server", "Unable to create new named pipe (%d).\n", GetLastError());



		/* connect the pipe instance */
		pipes[i].pending = server_connect(pipes[i].pipe, &pipes[i].overlapped);

		pipes[i].state = pipes[i].pending ? CONNECTING: READING;

	}


	while (TRUE) {

		i = WaitForMultipleObjects(INSTANCES, events, FALSE, INFINITE) - WAIT_OBJECT_0;

		if (i < 0  || i >= INSTANCES)
				ERR("Server","Invalid object recieved (%d).\n", GetLastError());


		/* If there's some pending operation */
		if (pipes[i].pending) {

			success = GetOverlappedResult(pipes[i].pipe,
										  &pipes[i].overlapped,
										  &size,
										  FALSE);


			switch (pipes[i].state) {

				case CONNECTING:
					if (!success)
						ERR("Server", "Unable to connect to client (%d).\n",
								GetLastError());

					pipes[i].state = READING;
					break;

				case READING:
					if (!success || size == 0) {

						server_recon(&pipes[i]);
						continue;

					}
					pipes[i].read = size;
					pipes[i].state = EXECUTING;
					break;

				case EXECUTING:
					pipes[i].state = READING;
					break;

			}
		}

		if (pipes[i].state == READING) {

			/* at this state we should be able to read from the pipe */
			success = ReadFile(pipes[i].pipe,
								pipes[i].request,
								BUFSIZE*sizeof(TCHAR),
								&pipes[i].read,
								&pipes[i].overlapped);

			pipes[i].request[pipes[i].read-1] = '\0';

			if (success && pipes[i].read != 0) {

				pipes[i].pending = FALSE;
				pipes[i].state = EXECUTING;
				continue;

			}

			if (!success && (GetLastError() == ERROR_IO_PENDING)) {

				pipes[i].pending = TRUE;
				continue;

			}

			server_recon(&pipes[i]);

		} else if (pipes[i].state == EXECUTING) {

			fn(pipes[i].request);
			server_recon(&pipes[i]);

		}

	}
}

int server_connect(HANDLE pipe, OVERLAPPED* pOverlapped)
{
	int pending=FALSE;

	if (ConnectNamedPipe(pipe, pOverlapped))
		ERR("Server", "Unable to connect to client (%d).\n",
				GetLastError());

	switch(GetLastError()) {

		case ERROR_IO_PENDING:
			pending = TRUE;
			break;

		case ERROR_PIPE_CONNECTED:
			if (SetEvent(pOverlapped->hEvent))
				break;

		default:
			ERR("Server","Unable to connect to client (%d).\n",
					GetLastError());
	}

	return pending;
}

void server_recon(pipeinst_t* pipe)
{
	if (! DisconnectNamedPipe(pipe->pipe))
		ERR("Server", "Unable to disconnect from client (%d).\n",
				GetLastError());

	pipe->pending = server_connect(pipe->pipe, &(pipe->overlapped));

	pipe->state = pipe->pending ? CONNECTING : READING;
}
