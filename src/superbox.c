/*

 SuperBox - A graphical console server for batchs
 Copyright (c) 2014, 2015 Romain GARBI (Darkbatcher)

 All rights reserved.
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 * Neither the name of the name of Romain Garbi (Darkbatcher) nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY ROMAIN GARBI AND CONTRIBUTORS ``AS IS'' AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL ROMAIN GARBI AND CONTRIBUTORS BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES
 LOSS OF USE, DATA, OR PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 Usage :

 		superbox [name | /l]

	Starts a superbox server

			- name : A name to be used as a prefix for the named pipe path.

			- /l   : Display the license.

	Once opened, superbox opens diffenrent named pipes for batch to comunicate
	with it. The pipe paths consists of the following syntax :

		\\.\pipe\name[postfix]

	Where postfix must be one of the following

		- keyw	: Input from keyboard, with wait.

		- key	: Input from keyboard without wait.

		- mousew: Input from mouse with wait

		- mouse : Input from mouse without wait

		- If there is no postfix, this is the main chain for writing at the
		console.
*/

#include "superbox.h"

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <tchar.h>
#include <wchar.h>

#define CMDLINEMAX (FILENAME_MAX+9)

#include "graphics.h"

int main(int argc, TCHAR* argv[])
{
	TCHAR *name,
		   cmd[CMDLINEMAX],
		   self[FILENAME_MAX];

	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	if (argc > 2) {

		_ftprintf(stderr, TEXT("SuperBox : Error : Wrong command line \n"));
		return -1;

	}

	if (argv[1]==NULL) {

		_tprintf(TEXT("superbox [-1] copyright (c) 2014, 2015 Romain Garbi\n"
			"This is free software, permission granted to modify and/or distribute copies\n"
			"under the terms of the 3 clause BSD license. To get more information, please\n"
			"type\"superbox /l\".\n"
			"\n"
			"SUPERBOX COMMAND\n"
			"\n"
			"     The superbox command provides a console server that enables batch scripts \n"
			"     to produce console graphics easily.\n"
			"\n"
			"     The superbox command is derived from the batbox command, but is designed \n"
			"     to adress the most important deficiencies of batbox and brings a lot of \n"
			"     inovations.\n"
			"\n"
			"SYNOPSIS\n"
			"\n"
			"        superbox [name | /l]\n"
			"\n"
			"     Launch a superbox console server.\n"
			"\n"
			"        - name : The named that should be used for named pipes used to \n"
			"          communicate with the launched server\n"
			"\n"
			"        - /l : Display superbox's license.\n"
			"\n"
			"     Once launched, the superbox server creates a named pipe to get programs \n"
			"     requests. The pipe name is \\.pipename, and can be accessed from any file \n"
			"     writing method.\n"
			"\n"
			"     When you access the pipe, you are suposed to send request to the server \n"
			"     trough the pipe. The request should be written using the following \n"
			"     syntax:\n"
			"\n"
			"        verb content\n"
			"\n"
			"     Typically, the content argument can't be easily described because \n"
			"     content's syntax heavilly depends on verb, although sending request is \n"
			"     quite straightforward :\n"
			"\n"
			"        echo /d \"my text\" > \\.\\pipe\\myserver\n"
			"\n"
			"LOW-LEVEL VERBS\n"
			"\n"
			"     Superbox offers a set of low-level verbs, which can be used to draw \n"
			"     directly in the console.\n"
			"\n"
			"        Full name    ab.     Full name    ab.     Full name    ab.\n"
			"        \n"
			"        /ascii        /a     /cusorheight /ch     /offset       /o\n"
			"        /clear       /cl     /goto         /g     /quit         /q\n"
			"        /clearrect  /clr     /display      /d     /showconsole  /s\n"
			"        /cleartext   /ct     /displaytext /dt     /title        /t\n"
			"        /color        /c     /hidecursor   /h\n"
			"        /colorrect   /cr     /newline      /n\n"
			"\n"
			"        - /ascii or /a : Display a character in the console. It is possible to \n"
			"          output any unicode character, as long as the font used by your \n"
			"          console support this character.\n"
			"\n"
			"             /ascii code\n"
			"\n"
			"             - code : The character's code, usually, the unicode codepoint of \n"
			"               the character if your console supports unicode character \n"
			"               output.\n"
			"\n"
			"        - /clear or /cl : Empty the console. Typically equivalent to cls \n"
			"          command in batch. No argument is needed.\n"
			"\n"
			"        - /clearrect or /clr : Empty a rectangle on the console. This is the \n"
			"          local version of the preceding verb.\n"
			"\n"
			"             /clearrect x y width height\n"
			"\n"
			"             - x, y : Coordinates (in cells) of the top-left corner of the \n"
			"               rectangle to clear.\n"
			"\n"
			"             - width, height : Dimensions (in cells) of the rectangle to \n"
			"               clear.\n"
			"\n"
			"        - /cleartext or /ct : Empty text in a rectangle. This verb has the \n"
			"          same effect and syntax as /clearrect, except that it only clears \n"
			"          text, colors are kept.\n"
			"\n"
			"        - /color or /c : Set console text color, without redefining the color \n"
			"          of the whole consoe.\n"
			"\n"
			"             /color code\n"
			"\n"
			"             - code : console color code to be set. Codes are the same for \n"
			"               batbox and superbox.\n"
			"\n"
			"        - /colorrect or /cr : Change color of a rectangle, without affecting \n"
			"          text contained in the rectangle.\n"
			"\n"
			"             /color code x y width height\n"
			"\n"
			"             - code : idem.\n"
			"\n"
			"             - x, y, width, height : Rectangle characteristics.\n"
			"\n"
			"        - /cursorheight or /ch : Changes cursor height.\n"
			"\n"
			"             /cursorheight height\n"
			"\n"
			"             - height : A number between 1 (thin line) and 100 (full cell) \n"
			"               that represents height of cursor in per cent.\n"
			"\n"
			"        - /goto or /g : Change cursor position.\n"
			"\n"
			"             /goto x y\n"
			"\n"
			"             - x, y : New coordinates (in cells) of the cursor.\n"
			"\n"
			"        - /display or /d : Output text in the console.\n"
			"\n"
			"             /display text\n"
			"\n"
			"             - text : text to output.\n"
			"\n"
			"        - /displaytext or /dt : Ouput text in the console without affecting \n"
			"          console colors.\n"
			"\n"
			"             /displaytext text\n"
			"\n"
			"             - text : idem.\n"
			"\n"
			"        - /hidecursor or /h : Hide or display the console cursor.\n"
			"\n"
			"             /hidecursor mode\n"
			"\n"
			"             - mode : 1 (hidden) or 0 (visible).\n"
			"\n"
			"        - /newline or /n : Produce a newline. Basically, if you let (x,y) be \n"
			"          the coordinates of the last /goto request, then, the cursor will be \n"
			"          moved at coordinates (x,y+1). No arguments.\n"
			"\n"
			"        - /offset or /o : Places the coordinates origin in the console.\n"
			"\n"
			"             /offset x y\n"
			"\n"
			"             - x, y : components (in cells) of the vector from console \n"
			"               top-left corner to the new origin.\n"
			"\n"
			"        - /quit or /q : Shutdown the server.\n"
			"\n"
			"        - /showconsole or /s : Select console window state.\n"
			"\n"
			"             /showconsole mode\n"
			"\n"
			"             - mode : Values that define state of the window. Both superbox \n"
			"               and batbox have the same values.\n"
			"\n"
			"        - /title or /t : Set console title.\n"
			"\n"
			"             /title titre\n"
			"\n"
			""
			   ));

		return 0;

	}

	if (!_tcsicmp(argv[1], TEXT("/l"))) {

		_tprintf(TEXT(
				"SuperBox - A graphical console server for batchs"
				"Copyright (c) 2014, 2015 Romain GARBI (Darkbatcher)\n"
				"\n"
				"All rights reserved.\n"
				"Redistribution and use in source and binary forms, with or without\n"
				"modification, are permitted provided that the following conditions are met:\n"
				"\n"
				" * Redistributions of source code must retain the above copyright\n"
				"   notice, this list of conditions and the following disclaimer.\n"
				" * Redistributions in binary form must reproduce the above copyright\n"
				"   notice, this list of conditions and the following disclaimer in the\n"
				"   documentation and/or other materials provided with the distribution.\n"
				" * Neither the name of the name of Romain Garbi (Darkbatcher) nor the\n"
				"   names of its contributors may be used to endorse or promote products\n"
				"   derived from this software without specific prior written permission.\n"
				"\n"
				"THIS SOFTWARE IS PROVIDED BY ROMAIN GARBI AND CONTRIBUTORS ``AS IS'' AND ANY\n"
				"EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED\n"
				"WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE\n"
				"DISCLAIMED. IN NO EVENT SHALL ROMAIN GARBI AND CONTRIBUTORS BE LIABLE FOR ANY\n"
				"DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES\n"
				"(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES\n"
				"LOSS OF USE, DATA, OR PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND\n"
				"ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT\n"
				"(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS\n"
				"SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.\n"
				"\n"
				 ));

				 return 0;

	}

	name = argv[1];

	if (!_tcsncmp(name, TEXT("\\\\.\\"), 3)) {

		graphics_init();
		server(name, graphics_exec, 0);

	} else {

		_sntprintf(cmd, CMDLINEMAX, TEXT("superbox \\\\.\\pipe\\%s"), name);
		GetModuleFileName(NULL, self, FILENAME_MAX);
		_tprintf(TEXT("Launching \"%s\": %s\n"), self, cmd);

		memset(&si, 0, sizeof(si));

		if (CreateProcess(self, cmd, NULL, NULL, FALSE, 0,
								NULL, NULL, &si, &pi) == 0)
			ERR("","Unable to launch the server.\n");

		CloseHandle(pi.hProcess);
		CloseHandle(pi.hThread);

	}

	return 0;
}
