/*

 SuperBox - A graphical console server for batchs
 Copyright (c) 2014, 2015 Romain GARBI (Darkbatcher)

 All rights reserved.
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 * Neither the name of the name of Romain Garbi (Darkbatcher) nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY ROMAIN GARBI AND CONTRIBUTORS ``AS IS'' AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES
 LOSS OF USE, DATA, OR PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef SUPERBOX_SERVER_H
#define SUPERBOX_SERVER_H

#include "superbox.h"

#include <stdio.h>
#include <stdlib.h>
#include <tchar.h>

#define CONNECTING 0
#define READING 1
#define EXECUTING 2
#define INSTANCES 52
#define PIPE_TIMEOUT 5000
#define BUFSIZE 4096

typedef struct
{
   OVERLAPPED 	overlapped;
   HANDLE 		pipe;
   TCHAR 		request[BUFSIZE];
   DWORD		read;
   TCHAR 		reply[BUFSIZE];
   DWORD   		toWrite;
   DWORD   		state;
   BOOL  		pending;
} pipeinst_t;

int server(TCHAR* name, void(*fn)(TCHAR*) , int type);
int server_connect(HANDLE pipe, OVERLAPPED* pOverlapped);
void server_recon(pipeinst_t* pipe);


#endif // SUPERBOX_SERVER
