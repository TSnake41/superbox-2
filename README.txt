SuperBox [version -1] - A graphical server for console

This package contains the following elements :

	- "superbox.exe" : The SuperBox binary.
	
	- "DOC.html" or "DOC.txt" : SuperBox documentation.

	- "COPYING.txt" : SuperBox license.
	
	- "src/" : Source folder.
	
To build the package, you need MinGW. Once in the MinGW shell, juste type :

	./configure && make
	
For bug reports or suggestions, please contact me at <darkbatcher@dos9.org>